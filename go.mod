module gitlab.com/noconcept.design/games-in-go

go 1.15

require (
	github.com/veandco/go-sdl2 v0.4.5-0.20200814083547-e1e6fddc529f
	golang.org/x/tools v0.0.0-20200925191224-5d1fdd8fa346 // indirect
)
