# PICA ÌLAD
*a tribute to **Pica**sso and **Dalí*** ;-)

---

## go und sdl2

[download go](https://golang.org/dl/)

[sdl2](https://www.libsdl.org/download-2.0.php)

---

### Bilder kombinieren, vermischen (Survivor)
Ein oder mehrere Bilder mit der Maus auswählen, und den unteren weissen Button klicken.

### Vergrössern (Zoom)
Bild mit der rechten Maustaste anklicken (WARTEN!!!)

### Speichern (Save)
Bild mit grosser S Taste speichern (SHIFT + s)

### gespeichertes Bild laden (Parse)
Die Bilder werden mit der .apt Endung gespeichert (1.apt, 2.apt, 3.apt ...), um das erste Bild zu laden:

    go run picailad.go 1.apt

### Fenstergrösse und Felder verändern
in der picailad.go die Zeilen 17 + 18 anpassen 


    // in diesem Beisiel 1700pixel breit und 900 pixel hoch
    var winWidth, winHeight int = 1700, 900

    // in diesem Beispiel 4 x 4 Felder
    var rows, cols, numPics int = 4, 4, rows * cols

### Nodes und Granulierung anpassen
In der picailad.go die Intn und +Node Werte in den Zeilen 59, 54 und 69 anpassen

    num := rand.Intn(15) + 5

    num = rand.Intn(13) + 5

    num = rand.Intn(14) + 5


ebenso in Zeile 152 kann der Intn Wert angepasst werden

    r := rand.Intn(15)


### Screenshot
![img1](./images/img1.png)

![img2](./images/img2.png)

![img3](./images/img3.png)

#### PICA ÌLAD - noconcept.design
build with the help of the youtube series "Games With Go" from [Jack Mott](https://www.youtube.com/channel/UCnHvMT-7FjTRPUGLVj9mgaA) 