package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

type choices struct {
	cmd         string
	description string
	nextNode    *storyNode
	nextChoice  *choices
}

type storyNode struct {
	text    string
	choices *choices
}

func (node *storyNode) addChoice(cmd string, description string, nextNode *storyNode) {
	choice := &choices{cmd, description, nextNode, nil}

	if node.choices == nil {
		node.choices = choice
	} else {
		currentChoice := node.choices
		for currentChoice.nextChoice != nil {
			currentChoice = currentChoice.nextChoice
		}
		currentChoice.nextChoice = choice
	}
}

func (node *storyNode) render() {
	fmt.Println(node.text)
	currentChoice := node.choices
	for currentChoice != nil {
		fmt.Println(currentChoice.cmd, ":", currentChoice.description)
		currentChoice = currentChoice.nextChoice
	}
}

func (node *storyNode) executeCmd(cmd string) *storyNode {
	currentChoice := node.choices
	for currentChoice != nil {
		if strings.ToLower(currentChoice.cmd) == strings.ToLower(cmd) {
			return currentChoice.nextNode
		}
		currentChoice = currentChoice.nextChoice
	}
	fmt.Println("Oops, das habe ich nicht verstanden.")
	return node
}

var scanner *bufio.Scanner

func (node *storyNode) play() {
	node.render()
	if node.choices != nil {
		scanner.Scan()
		node.executeCmd(scanner.Text()).play()
	}
}

func main() {
	scanner = bufio.NewScanner(os.Stdin)

	start := storyNode{text: `
	Du befindest dich in einem grossen Gewölbe, tief unter der Erde! *schauder*.
	Du siehst drei Passagen vor Dir. 
	Die Nord-Passage führt hinunter ins dunkle.
	Die Süd-Passage scheint aufwärts zu führen. 
	Die Ost-Passage erscheint eben und viel bewandert.`}

	darkRoom := storyNode{text: "Es ist stockdunkel. Du siehst rein garnichts."}

	darkRoomLit := storyNode{text: "Die dunkle Passage wurde durch deine Laterne erhellt. Du kannst weitergehen in Richtung Nord, oder zurück nach Süden"}

	grue := storyNode{text: "Während Du so durch die Dunkelheit stolperst, wurdest Du von einem Monster gefressen."}

	trap := storyNode{text: "Du bist den ebenen und viel bewanderten Pfad gelaufen , als sich plötzlich eine Falltür öffnet und Du in die Grube fällst."}

	treasure := storyNode{text: "Du hast eine kleine versteckte Niesche gefunden,.... voll mit Diamanten, Gold und einem TraumJob"}

	start.addChoice("N", "Gehe nach Norden", &darkRoom)
	start.addChoice("S", "Gehe nach Süden", &darkRoom)
	start.addChoice("O", "Gehe nach Osten", &trap)

	darkRoom.addChoice("S", "Versuche zurück nach Süden zu gehen.", &grue)
	darkRoom.addChoice("L", "Die vor Dir liegende Laterne nehmen, und anschalten", &darkRoomLit)

	darkRoomLit.addChoice("N", "Gehe nach Norden", &treasure)
	darkRoomLit.addChoice("S", "Gehe nach Süden", &start)

	start.play()

	fmt.Println()
	fmt.Println("Ende")

}
