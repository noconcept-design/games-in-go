package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

type choice struct {
	cmd         string
	description string
	nextNode    *storyNode
}

type storyNode struct {
	text    string
	choices []*choice
}

func (node *storyNode) addChoice(cmd string, description string, nextNode *storyNode) {
	choice := &choice{cmd, description, nextNode}
	node.choices = append(node.choices, choice)
}

func (node *storyNode) render() {
	fmt.Println(node.text)
	if node.choices != nil {
		for _, choice := range node.choices {
			fmt.Println(choice.cmd, choice.description)
		}
	}
}

func (node *storyNode) executeCmd(cmd string) *storyNode {
	for _, choice := range node.choices {
		if strings.ToLower(choice.cmd) == strings.ToLower(cmd) {
			return choice.nextNode
		}
	}
	fmt.Println("Oops, das habe ich nicht verstanden.")
	return node
}

var scanner *bufio.Scanner

func (node *storyNode) play() {
	node.render()
	if node.choices != nil {
		scanner.Scan()
		node.executeCmd(scanner.Text()).play()
	}
}

func main() {

	scanner = bufio.NewScanner(os.Stdin)

	start := storyNode{text: `
	Du befindest dich in einem grossen Gewölbe, tief unter der Erde! *schauder*.
	Du siehst drei Passagen vor Dir. 
	Die Nord-Passage führt hinunter ins dunkle.
	Die Süd-Passage scheint aufwärts zu führen. 
	Die Ost-Passage erscheint eben und viel bewandert.`}

	darkRoom := storyNode{text: "Es ist stockdunkel. Du siehst rein garnichts."}

	darkRoomLit := storyNode{text: "Die dunkle Passage wurde durch deine Laterne erhellt. Du kannst weitergehen in Richtung Nord, oder zurück nach Süden"}

	grue := storyNode{text: "Während Du so durch die Dunkelheit stolperst, wurdest Du von einem Monster gefressen."}

	trap := storyNode{text: "Du bist den ebenen und viel bewanderten Pfad gelaufen , als sich plötzlich eine Falltür öffnet und Du in die Grube fällst."}

	treasure := storyNode{text: "Du hast eine kleine versteckte Niesche gefunden,.... voll mit Diamanten, Gold und einem TraumJob"}

	start.addChoice("N", "Gehe nach Norden", &darkRoom)
	start.addChoice("S", "Gehe nach Süden", &darkRoom)
	start.addChoice("O", "Gehe nach Osten", &trap)

	darkRoom.addChoice("S", "Versuche zurück nach Süden zu gehen.", &grue)
	darkRoom.addChoice("L", "Die vor Dir liegende Laterne nehmen, und anschalten", &darkRoomLit)

	darkRoomLit.addChoice("N", "Gehe nach Norden", &treasure)
	darkRoomLit.addChoice("S", "Gehe nach Süden", &start)

	start.play()

	fmt.Println()
	fmt.Println("Ende")

}
