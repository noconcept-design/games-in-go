package main

import (
	"gitlab.com/noconcept.design/games-in-go/rpg/game"
	"gitlab.com/noconcept.design/games-in-go/rpg/ui2d"
)

func main() {
	ui := &ui2d.UI2d{}
	game.Run(ui)
}
