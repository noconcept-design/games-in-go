ackage main

import (
	"fmt"
	"os"
	"time"

	"github.com/veandco/go-sdl2/img"
	"github.com/veandco/go-sdl2/sdl"
)

const (
	screenWidth  = 600
	screenHeight = 800

	// game speed
	targetTicksPerSecond = 60
)

type vector struct {
	x float64
	y float64
}

//make game speed on every machine the same
var delta float64

func main() {
	if err := sdl.Init(sdl.INIT_EVERYTHING); err != nil {
		fmt.Fprintf(os.Stderr, "Failed to inizialierung von SDL: %S\n", err)
		return
	}

	window, err := sdl.CreateWindow(
		" SuupaSta - by NOCONCEPT DESIGN",
		sdl.WINDOWPOS_UNDEFINED, sdl.WINDOWPOS_UNDEFINED,
		screenWidth, screenHeight,
		sdl.WINDOW_OPENGL)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to initializing window: %s\n", err)
		return
	}
	defer window.Destroy()

	renderer, err := sdl.CreateRenderer(window, -1, sdl.RENDERER_ACCELERATED)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to inizializing renderer: %s\n", err)
		return
	}
	defer renderer.Destroy()

	sdl.SetHint(sdl.HINT_RENDER_SCALE_QUALITY, "1")
	surfaceImg, err := img.Load("sprites/background.png")
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to load PNG: %s\n", err)
		return
	}

	defer surfaceImg.Free()

	textureImg, err := renderer.CreateTextureFromSurface(surfaceImg)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to create texture: %s\n", err)
		return
	}

	defer textureImg.Destroy()

	elements = append(elements, newPlayer(renderer))

	for i := 0; i < 5; i++ {
		for j := 0; j < 3; j++ {
			x := (float64(i)/5)*screenWidth + (basicEnemySize / 2.0)
			y := float64(j)*basicEnemySize + (basicEnemySize / 2.0)

			enemy := newBasicEnemy(renderer, x, y)
			elements = append(elements, enemy)
		}
	}

	initBulletPool(renderer)
	var angle float64 = 0.0
	for {
		// game speed calc
		frameStartTime := time.Now()
		// listen for window close etc.
		for event := sdl.PollEvent(); event != nil; event = sdl.PollEvent() {
			switch event.(type) {
			case *sdl.QuitEvent:
				return
			}
		}
		renderer.SetDrawColor(255, 255, 255, 255)

		renderer.Clear()

		//textureImg * sdl.Renderer(renderer.Copy(textureImg, nil, &sdl.Rect{0, 350, imageWidth, imageHeight}))
		//renderer.FillRect(&sdl.Rect{0, 0, int32(screenWidth), int32(screenHeight)})
		angle += 0.1
		if angle > 360.0 {
			angle = 0
		}
		// renderer.Copy(textureImg, nil, &sdl.Rect{0, 330, screenWidth, screenHeight / 2}, angle, nil, sdl.FLIP_NONE)
		renderer.Copy(textureImg, nil, &sdl.Rect{0, 330, screenWidth, screenHeight / 2})

		for _, elem := range elements {
			if elem.active {
				err = elem.update()
				if err != nil {
					fmt.Println("updating element:", err)
					return
				}
				err = elem.draw(renderer)
				if err != nil {
					fmt.Println("drawing element:", err)
					return
				}
			}
		}

		if err := checkCollision(); err != nil {
			fmt.Println("checking collisions:", err)
			return
		}

		renderer.Present()

		// game speed
		delta = time.Since(frameStartTime).Seconds() * targetTicksPerSecond
	}

}
